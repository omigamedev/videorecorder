#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <chrono>
#include <thread>

extern "C"
{
    #include <libavformat/avformat.h>
    #include <libavcodec/avcodec.h>
    #include <libavutil/avutil.h>
    #include <libswscale/swscale.h>
}