#include "stdafx.h"
#include <assert.h>

int main()
{
    av_register_all();

    AVFormatContext* ctx = nullptr;
    avformat_alloc_output_context2(&ctx, NULL, "mp4", "out.mp4");

    AVOutputFormat* fmt = ctx->oformat;

    auto codec = avcodec_find_encoder(fmt->video_codec);
    printf("encoder: %s\n", avcodec_get_name(fmt->video_codec));

    auto stream = avformat_new_stream(ctx, codec);
    assert(stream != nullptr);
    stream->id = 0;
    stream->time_base = { 1, 30 };
    stream->codec->codec_id = fmt->video_codec;
    stream->codec->bit_rate = 200000;
    stream->codec->width = 800;
    stream->codec->height = 600;
    stream->codec->time_base = stream->time_base;
    stream->codec->gop_size = 12;
    stream->codec->pix_fmt = AVPixelFormat::AV_PIX_FMT_YUV420P;
    /* Some formats want stream headers to be separate. */
    if (ctx->oformat->flags & AVFMT_GLOBALHEADER)
        stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    AVDictionary* opt = nullptr;
    avcodec_open2(stream->codec, codec, &opt);

    AVFrame* frameRGB = av_frame_alloc();
    frameRGB->format = AVPixelFormat::AV_PIX_FMT_RGB24;
    frameRGB->width = stream->codec->width;
    frameRGB->height = stream->codec->height;
    av_frame_get_buffer(frameRGB, 32);

    AVFrame* frameYUV = av_frame_alloc();
    frameYUV->format = AVPixelFormat::AV_PIX_FMT_YUV420P;
    frameYUV->width = stream->codec->width;
    frameYUV->height = stream->codec->height;
    av_frame_get_buffer(frameYUV, 32);

    /* check if we want to generate more frames */
    //if (av_compare_ts(ost->next_pts, ost->st->codec->time_base,
    //    10.0, { 1, 1 }) >= 0)
    //    return NULL;

    auto sws = sws_getContext(frameRGB->width, frameRGB->height, AV_PIX_FMT_RGB24,
        frameYUV->width, frameYUV->height, AV_PIX_FMT_YUV420P, SWS_BICUBIC, 0, 0, 0);

    avio_open(&ctx->pb, "out.mp4", AVIO_FLAG_WRITE);
    avformat_write_header(ctx, &opt);

    int64_t next_pts = 0;

    av_frame_make_writable(frameRGB);

    auto start = std::chrono::steady_clock::now();
    float elapsed_time = 0;
    float timer = 0;
    float ms = 1.0 / 30.0;
    bool first_frame = true;
    for (int i = 0; i < 450; i++)
    {
        auto now = std::chrono::steady_clock::now();
        std::chrono::duration<float> diff = now - start;
        start = now;
        float dt = diff.count();
        timer += dt;
        elapsed_time += dt;

        if (timer < ms)
            std::this_thread::sleep_for(std::chrono::duration<float>(ms-timer));

        timer -= ms;

        if (first_frame)
        {
            first_frame = false;
            for (int y = 0; y < frameRGB->height; y++)
            {
                for (int x = 0; x < frameRGB->width; x++)
                {
                    int offset = 3 * (x + y * frameRGB->width);
                    frameRGB->data[0][offset + 0] = ((x + i) / 10) % 255; // R
                    frameRGB->data[0][offset + 1] = 0; // G
                    frameRGB->data[0][offset + 2] = 0; // B
                }
            }

            // moving white pixel
            {
                int offset = 3 * (i % (frameRGB->height * frameRGB->width));
                frameRGB->data[0][offset + 0] = 255; // R
                frameRGB->data[0][offset + 1] = 255; // G
                frameRGB->data[0][offset + 2] = 255; // B
            }
        }

            // Convert frame from RGB to YUV
            sws_scale(sws, frameRGB->data, frameRGB->linesize, 0, frameRGB->height,
                frameYUV->data, frameYUV->linesize);

        int frameEncoded = 0;
        AVPacket pkt{ 0 };
        av_init_packet(&pkt);
        frameYUV->pts = next_pts++;
        avcodec_encode_video2(stream->codec, &pkt, frameYUV, &frameEncoded);
        if (frameEncoded)
        {
            av_packet_rescale_ts(&pkt, stream->codec->time_base, stream->time_base);
            pkt.stream_index = stream->index;
            av_interleaved_write_frame(ctx, &pkt);
            printf(".");
        }
        else
        {
            printf("frame NOT encoded\n");
        }
    }

    int frameEncoded = 0;
    do 
    {
        AVPacket pkt{ 0 };
        av_init_packet(&pkt);
        avcodec_encode_video2(stream->codec, &pkt, nullptr, &frameEncoded);
        if (frameEncoded)
        {
            av_packet_rescale_ts(&pkt, stream->codec->time_base, stream->time_base);
            pkt.stream_index = stream->index;
            av_interleaved_write_frame(ctx, &pkt);
            printf("delayed frame\n");
        }
    } while (frameEncoded);

    printf("elapsed %.2f seconds\n", elapsed_time);

    av_write_trailer(ctx);
    avio_closep(&ctx->pb);

    system("pause");
    return 0;
}

